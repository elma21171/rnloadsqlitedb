import React, {useState, useEffect} from 'react';
import {Text, View, FlatList} from 'react-native';
import SQLite from 'react-native-sqlite-storage';

SQLite.DEBUG(true);
SQLite.enablePromise(true);

const Test = () => {
  let [flatListItems, setFlatListItems] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const db = SQLite.openDatabase({
        name: 'wordsDB',
        location: 'default',
      });

      try {
        db.transaction(txn => {
          console.log('Database open !');

          txn.executeSql('SELECT * FROM singlewords_m', [], (tx, results) => {
            console.log('Database execute!');

            let temp = [];
            for (let i = 0; i < results.rows.length; i++) {
              console.log('length ', results.rows.length);

              temp.push(results.rows.item(i));
              setFlatListItems(temp);
              console.log(temp);
            }
          });
        });
      } catch (err) {
        console.log(err);
      }
    };

    getData();
  }, []);

  return (
    <>
      <Text>{flatListItems.length}</Text>
      <FlatList
        data={flatListItems}
        renderItem={({item}) => (
          <View key={item.id} style={{padding: 20}}>
            <Text>Colnum: {item.colnum}</Text>
            <Text>Term: {item.term}</Text>
            <Text>Term4search: {item.term4search}</Text>
          </View>
        )}
        keyExtractor={item => item.id}
      />
    </>
  );
};

export default Test;
