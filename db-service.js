import {
    enablePromise,
    openDatabase,
    SQLiteDatabase,
  } from 'react-native-sqlite-storage';
  
  enablePromise(true);
  
  export const getDBConnection = async () => {
    return openDatabase({name: 'wordsDB.db', location: 'default'});
  };
  
  export const getWords = async db => {
    try {
      const wordsList = [];
      const results = await db.executeSql(`SELECT * FROM singlewords_m`);
      results.forEach(result => {
        for (let index = 0; index < result.rows.length; index++) {
          wordsList.push(result.rows.item(index));
        }
      });
      return wordsList;
    } catch (error) {
      console.error(error);
      throw Error('Failed to get wordsList !!!');
    }
  };
  