import React, {useCallback, useEffect, useState} from 'react';
import {Text, View, FlatList} from 'react-native';
import {getDBConnection, getWords} from './db-service';

function App(): JSX.Element {
  const [words, setWords] = useState([{id: 0, colnum: 0,term:'', term4search:''}]);

  const loadDataCallback = async () => {
    try {
      const db = await getDBConnection();
      const storedWords = await getWords(db);
      setWords(storedWords);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    loadDataCallback();
  }, []);

  return (
    <View style={{padding: 20}}>
      <Text>hi</Text>
      
      <Text>{words.length}</Text>

      <FlatList
        data={words}
        renderItem={({item}) => (
          <View key={item.id} style={{padding: 20}}>
            <Text>Colnum: {item.colnum}</Text>
            <Text>Term: {item.term}</Text>
            <Text>Term4search: {item.term4search}</Text>
          </View>
        )}
        
      />
    </View>
  );
}

export default App;
